# README #


### How do I get set up? ###

1. Abrir terminal e ir a la ruta donde se desea almacenará el proyecto(este creara una carpeta con el nombre al clonarlo)
2. clonar con el comando: 
```bash
git clone https://madwyatt@bitbucket.org/madwyatt/meteor-rect-chat.git
```
3. Luego entrar al directorio clonado:
```bash
cd meteor-rect-chat
```
4.Instalar dependencias con:
```bash
meteor npm i
```
5. Correr la aplicaion con:
```bash
meteor
```
6. Abrirla en el navegador
[http://localhost:3000](http://localhost:3000)
