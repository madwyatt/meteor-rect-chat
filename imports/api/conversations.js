import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import moment from 'moment';



export const Conversations = new Mongo.Collection('conversations');

if (Meteor.isServer) {
    Meteor.publish('conversations', () => {
        return Conversations.find();
    });
}

Meteor.methods({
    'conversation.insert'(message) {
        check(message, {
            text: String,
            idReceiver: String,
            createdAt: Date
        });

        // Make sure the user is logged in before inserting a task
        if (!this.userId) {
            throw new Meteor.Error('not-authorized');
        }

        Conversations.insert({
            text: message.text,
            idSender: this.userId,
            idReceiver: message.idReceiver,
            createdAt: message.createdAt,
        });
    },

});

