import { Meteor } from 'meteor/meteor';


if (Meteor.isServer) {
    Meteor.publish('users-all', () => {
        return Meteor.users.find({}, { fields: { _id: 1, username: 1, createdAt: 1 } });
    });
}