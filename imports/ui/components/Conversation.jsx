import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import ReactDOM from 'react-dom';


import Message from "./Message";
import { Conversations } from "../../api/conversations.js";


class Conversation extends Component {

    renderMessages() {
        //console.log('asd:', this.props.actual);
        return this.props.messages.map((message) => {
            try {
                // caso 1: idReceiver= receiberUser, caso 2:id sender=receiberUser
                if ((message.idReceiver == this.props.actual._id && message.idSender==this.props.currentUserId) ||
                     (message.idSender == this.props.actual._id && message.idReceiver==this.props.currentUserId)) {
                    return (<Message key={message._id} m={message} />);
                }
                else return ('');
            } catch (error) {
                return ('');
            }
        });
    }

    componentDidUpdate(prevProps, prevState) {
        const div = ReactDOM.findDOMNode(this.refs.con);
        div.scrollTop = div.scrollHeight - div.clientHeight;
    }

    render() {
        // console.log(this.props.actual);
        if (!this.props.currentUserId)
            return (
                <div className="jumbotron">
                    <p className="header">Ingresa para enviar mensajes</p>
                </div>
            )
        else if (!Session.get("receiberUserid")) {
            return (
                <div className="col-sm-8 conversation">
                    {/* Heading */}
                    <div className="row heading">
                        <div className="col-sm-2 col-md-1 col-xs-3 heading-avatar">
                            <div className="heading-avatar-icon">
                                <img src="/no-user.png" />
                            </div>
                        </div>
                        <div className="col-sm-9 col-xs-8 heading-name">
                            <a className="heading-name-meta">Selecciona un Usuario para Chatear</a>
                            <span className="heading-online">Online</span>
                        </div>
                    </div>
                    {/* Heading End */}
                    {/* Message Box */}
                    <div ref="con" className="row message" id="conversation">

                    </div>
                    {/* Reply Box */}
                    <div className="row reply">
                        
                    </div>
                    {/* Reply Box End */}
                </div>
            )
        } else {
            return (
                <div className="col-sm-8 conversation">
                    {/* Heading */}
                    <div className="row heading">
                        <div className="col-sm-2 col-md-1 col-xs-3 heading-avatar">
                            <div className="heading-avatar-icon">
                                <img src="/no-user.png" />
                            </div>
                        </div>
                        <div className="col-sm-9 col-xs-8 heading-name">
                            <a className="heading-name-meta">{(this.props.actual) ? this.props.actual.username : ""}</a>
                            <span className="heading-online">Online</span>
                        </div>
                    </div>
                    {/* Heading End */}
                    {/* Message Box */}
                    <div ref="con" className="row message" id="conversation">
                        {this.renderMessages()}
                    </div>
                    {/* Reply Box */}
                    <div className="row reply">
                        <form onSubmit={this.handleSubmit.bind(this)}>
                            <div className="col-sm-10 col-xs-10 reply-main">
                                <input
                                    ref="textArea"
                                    type="text"
                                    className="form-control"
                                    /* rows={1} id="comment" */
                                    /* defaultValue={""} */
                                    placeholder="Escriba su mensaje..." />
                            </div>
                            <div className="col-sm-1 col-xs-1 reply-send">
                                <button className="btn btn-link" type="submit"><i className="fa fa-send fa-2x" aria-hidden="true" /></button>
                            </div>
                        </form>
                    </div>
                    {/* Reply Box End */}
                </div>
            )
        }

    }

    handleSubmit(event) {
        event.preventDefault();
        // Find the text field via the React ref
        const text = ReactDOM.findDOMNode(this.refs.textArea).value.trim();
        const msg = {
            text,
            idReceiver: this.props.actual._id,
            createdAt: new Date()
        };
       // console.log(msg.createdAt.toString());
        Meteor.call('conversation.insert', msg);
        ReactDOM.findDOMNode(this.refs.textArea).value = '';
    }
}
export default withTracker(() => {
    Meteor.subscribe('conversations').ready();
    Meteor.subscribe('users-all').ready();
    const actual = Meteor.users.findOne({ _id: Session.get("receiberUserid") });

    return {
        actual,
        messages: Conversations.find().fetch(),
        currentUserId: Meteor.userId(),
        receiberUser: Session.get("receiberUserid"),
    };
})(Conversation);