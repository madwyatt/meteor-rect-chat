import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';

import User from "./User";
import { Users } from "../../api/users.js";

class UserList extends Component {

    renderUsers() {
        if (this.props.usersAll.length <= 1) {
            return 'No hay mas usuarios';
        } else {
            return this.props.usersAll.map((user) => (
                (this.props.currentUser != user._id) ?
                    <User key={user._id} user={user} />
                    : ''
            ));
        }

    }

    render() {
        return (
            <div className="row sideBar">
                {(this.props.currentUser) ? this.renderUsers() : ''}
            </div>
        )
    }
}

export default withTracker(() => {
    Meteor.subscribe('users-all').ready();
    const usersAll = Meteor.users.find({}, { fields: { _id: 1, username: 1 } }).fetch();
    // console.log(Meteor.user());
    return {
        usersAll,
        currentUser: Meteor.userId(),
        receiberUser: Session.get("receiberUserid"),
    };
})(UserList);