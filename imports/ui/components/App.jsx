import React, { Component } from 'react';
import AccountsUIWrapper from "./AccountsUIWrapper";
import { withTracker } from 'meteor/react-meteor-data';

import UserList from "./UserList";
import Conversation from "./Conversation";

// App component - represents the whole app
class App extends Component {

  render() {
    return (
      <div className="container app">
        <div className="row app-one">
          <div className="col-sm-4 side">
            <div className="side-one">
              {/* Heading */}
              <div className="row heading">
                <div className="col-sm-3 col-xs-3 heading-avatar">
                  <div className="heading-avatar-icon">
                    <img src="/no-user1.png" />
                  </div>
                </div>
                <div className="heading-avatar-user col-sm-7">
                  <AccountsUIWrapper />
                </div>
              </div>
              {/* Heading End */}

              {/* sideBar Users */}
              <div className="row sideBar">
                <UserList />
              </div>
            </div>

          </div>
          {/* Sidebar End */}
          {
            (this.props.currentUser!=null) ?
              <Conversation />
              : ''
          }
        </div>
        {/* App One End */}
      </div>

    );
  }
}

export default withTracker(() => {
  // console.log(Meteor.user());
  return {
    currentUser: Meteor.userId(),
  };
})(App);