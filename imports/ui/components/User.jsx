import React,{ Component } from "react";
import { Meteor } from 'meteor/meteor';

import { Conversations } from "../../api/conversations.js";

export default class User extends Component{
    clicUser(event){
        Session.set("receiberUserid", this.props.user._id)
    }

    render() {
        return (
            <div className="row sideBar-body" onClick={this.clicUser.bind(this)}>
                <div className="col-sm-3 col-xs-3 sideBar-avatar">
                    <div className="avatar-icon">
                        <img src="/no-user.png" />
                    </div>
                </div>
                <div className="col-sm-9 col-xs-9 sideBar-main">
                    <div className="row">
                        <div className="col-sm-8 col-xs-8 sideBar-name">
                            <span className="name-meta">{ this.props.user.username }
                  </span>
                        </div>
                        <div className="col-sm-4 col-xs-4 pull-right sideBar-time">
                            <span className="time-meta pull-right">{/* 18:18 */}
                  </span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}