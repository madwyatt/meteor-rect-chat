import React, { Component } from 'react';
import moment from 'moment';
import { withTracker } from 'meteor/react-meteor-data';


class Message extends Component {
    constructor(props) {
        super(props);
        this.state = { date: this.props.m.createdAt }
    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
            date: this.props.m.createdAt
        });
    }

    render() {
        return (
            <div className="row message-body">
                <div className="col-sm-12 message-main-receiver">
                    <div className={
                        (this.props.m.idReceiver == Meteor.userId()) ? 'receiver' : 'sender'
                    }>
                        <div className="message-text">
                            {this.props.m.text}
                        </div>
                        <span className="message-time pull-right">
                            {moment(this.state.date).fromNow()}
                        </span>
                    </div>
                </div>
            </div>
        )
    }
}

export default Message;